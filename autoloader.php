<?php
/**
 * Created by PhpStorm.
 * User: mute
 * Date: 17.05.18
 * Time: 09:10
 */

if (file_exists('vendor/autoload.php')) {
    require('vendor/autoload.php');
}

spl_autoload_register(function ($className) {

    $namespace = 'AppBundle';

    if (strpos($className, $namespace) === 0) {
        $className = str_replace($namespace, '', $className);
        $fileName = __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
        if (file_exists($fileName)) {
            require($fileName);
        }
    }
});
