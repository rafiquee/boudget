<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 09:54
 */

namespace AppBundle\CommonObjects;

//Abstract class extends ValueObjectTest and can overwrite new methods
use AppBundle\Interfaces\ValueObjectInterface;

abstract class CommonValueObjects implements ValueObjectInterface
{
    /**
     * @var mixed|string
     */
    protected $value;

    public function __toString()
    {
        return (string)($this->value);
    }
}