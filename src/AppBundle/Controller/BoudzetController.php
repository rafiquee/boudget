<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-28
 * Time: 09:09
 */

namespace AppBundle\Controller;


use AppBundle\BoudgetModel;
use AppBundle\Lib\EntityManager;
use AppBundle\Lib\MySqlManager;
use AppBundle\StatisticModel;
use AppBundle\TransportObjects\RequestBoudgetValueInsert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entities\DateEntity;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class BoudzetController extends Controller
{
    /**
     * @Route("/boudget", name="budzet")
     */
    public function indexAction(Request $request)
    {
        if(!isset($_SESSION['login'])) { return $this->redirectToRoute('login'); }
        if(isset($_SESSION['boudgetValue'])) unset($_SESSION['boudgetValue']);

        $Manager = new EntityManager();
        $MySql = new MySqlManager();
        $date = new DateEntity();
        $ModelStat = new StatisticModel();
        $ModelBoudget = new BoudgetModel();

        $totalExpense = $ModelStat->ToEstimateTotalExpense('total_cena');
        $login = $_SESSION['login'];
        $total = $MySql->oneRecord('SELECT total FROM `'.$login.'.total` ORDER BY ID DESC LIMIT 1');
        $totalMonth = (float)$total[0];
        if($totalMonth==0) $totalMonth = 1;
        $percentValue = round(($totalExpense/$totalMonth)*100);
        if($percentValue>100)  $percentValue = 100; elseif ($percentValue<0) $percentValue = 0;

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            if($ModelBoudget->HtmlFormValidator())
            {
                $BoudgetValueToInsert = new RequestBoudgetValueInsert();
                $BoudgetValueToInsert->prepareFromArray($_POST);

                $Manager->insert($BoudgetValueToInsert);
                return $this->redirectToRoute('budzet');
            }

        }
        (isset($_SESSION['boudgetValue'])) ? $boudgetStat = $_SESSION['boudgetValue'] : $boudgetStat = '';

        return $this->render('budzet/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'date' => $date->getMonthName(),
            'fullDate' => $date->getFullDate(),
            'totalExpense' => $totalExpense,
            'totalMonth' => $totalMonth,
            'percentValue' => $percentValue,
            'boudgetStat' => $boudgetStat,
        ]);
    }
}