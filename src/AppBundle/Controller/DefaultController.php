<?php

namespace AppBundle\Controller;


use AppBundle\Entities\DateEntity;
use AppBundle\IndexModel;
use AppBundle\Lib\MySqlManager;
use AppBundle\StatisticModel;
use AppBundle\TransportObjects\RequestWordInsert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $date = new DateEntity();
        $Manager = new MySqlManager();
        $ModelIndex = new IndexModel();
        $ModelStat = new StatisticModel();

        if(!isset($_SESSION['login'])) {return $this->redirectToRoute('login');}

        $session->getFlashBag()->get('login');
        $login = $session->get('login');
        $results = $Manager->fetch_all('SELECT * FROM `'.$login.'.'.$date->getMonthName().'`');

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            if($ModelIndex->HtmlFormValidator())
            {
                $RecordToInsert = new RequestWordInsert();
                $RecordToInsert->prepareFromArray($_POST);
                $ModelIndex->insertRecordToDB($RecordToInsert);
                return $this->redirectToRoute('index');
            }
        }

        return $this->render('index/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'date' => $date->getMonthName(),
            'fullDate' => $date->getFullDate(),
            'results' => $results,
            'totalExpense' => $ModelStat->ToEstimateTotalExpense('total_cena'),
            'login' => $login,
        ]);
    }

    /**
     * @Route("/logout", name="log")
     */
    public function logout(Request $request)
    {
        unset($_SESSION['login']);
        return $this->redirectToRoute('login');
    }

}

