<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-23
 * Time: 17:59
 */

namespace AppBundle\Controller;


use AppBundle\Entities\DateEntity;
use AppBundle\LoginModel;
use AppBundle\TransportObjects\RequestLoginUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function indexAction(Request $request)
    {
        $date = new DateEntity();
        $model = new LoginModel();

        if(isset($_SESSION['login'])) { return $this->redirectToRoute('index'); }

        //to clear values of last session
        if(isset($_SESSION['login_status'])) unset($_SESSION['login_status']);
        if(isset($_SESSION['password_status'])) unset($_SESSION['password_status']);

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            if($model->loginFormValidator())
            {
                $user = new RequestLoginUser();
                $user->prepareFromArray($_POST);

                if ($user->isValid()) $model->login($user);
                return $this->redirectToRoute('index');
            }
        }

        (isset($_SESSION['login_status'])) ? $loginStat = $_SESSION['login_status'] : $loginStat = '';
        (isset($_SESSION['password_status'])) ? $passwordStat = $_SESSION['password_status'] : $passwordStat = '';

        return $this->render('login/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'date'=> 'login',
            'loginStat'=> $loginStat,
            'passStat'=> $passwordStat,
            'fullDate'=> $date->getFullDate(),
        ]);
    }
}