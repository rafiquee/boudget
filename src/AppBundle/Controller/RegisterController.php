<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-23
 * Time: 18:07
 */

namespace AppBundle\Controller;


use AppBundle\Entities\DateEntity;
use AppBundle\RegisterModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\TransportObjects\RequestRegisterUser;

class RegisterController extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function indexAction(Request $request)
    {
        if(isset($_SESSION['login'])) { return $this->redirectToRoute('index'); }

        //to clear values of last session
        if(isset($_SESSION['login_status'])) unset($_SESSION['login_status']);
        if(isset($_SESSION['password_status'])) unset($_SESSION['password_status']);
        if(isset($_SESSION['email_status'])) unset($_SESSION['email_status']);
        if(isset($_SESSION['email_repeat_status'])) unset($_SESSION['email_repeat_status']);
        if(isset($_SESSION['finallResult'])) unset($_SESSION['finallResult']);

        $date = new DateEntity();
        $model = new RegisterModel();

        if (($_SERVER['REQUEST_METHOD'] == 'POST'))
        {
            if($model->HtmlFormValidator())
            {
                $user = new RequestRegisterUser();
                $user->prepareFromArray($_POST);

                if ($user->isValid()) { $model->register($user); $_SESSION['finallResult'] = 'The registration process has been completed'; }
                else
                    echo "<h3 style='color: red'>The registration process has been not completed</h3>";

                return $this->redirectToRoute('index');
            }
        }

        //html validator - status
        (isset($_SESSION['login_status'])) ? $loginStat = $_SESSION['login_status'] : $loginStat = '';
        (isset($_SESSION['password_status'])) ? $passwordStat = $_SESSION['password_status'] : $passwordStat = '';
        (isset($_SESSION['email_status'])) ? $emailStat = $_SESSION['email_status'] : $emailStat = '';
        (isset($_SESSION['email_repeat_status'])) ? $emailRepStat = $_SESSION['email_repeat_status'] : $emailRepStat = '';
        (isset($_SESSION['finallResult'])) ? $finallResult = $_SESSION['finallResult'] : $finallResult = '';

        return $this->render('register/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'date'=> 'register',
            'loginStat'=> $loginStat,
            'passStat'=> $passwordStat,
            'emailStat'=> $emailStat,
            'emailRepStat'=> $emailRepStat,
            'finallResult'=> $finallResult,
            'fullDate'=> $date->getFullDate(),
        ]);
    }
}