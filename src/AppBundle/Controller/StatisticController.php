<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-23
 * Time: 17:59
 */

namespace AppBundle\Controller;


use AppBundle\Entities\DateEntity;
use AppBundle\StatisticModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;



class StatisticController extends Controller
{
    /**
     * @Route("/statistic", name="statistic")
     */
    public function indexAction(Request $request)
    {
        if(!isset($_SESSION['login'])) { return $this->redirectToRoute('login'); }

        $date = new DateEntity();
        $ModelStatistic = new StatisticModel();

        $totalArt = $ModelStatistic->ToEstimateTotalExpense('art_cena');
        $totalRtv = $ModelStatistic->ToEstimateTotalExpense('rtv_cena');
        $totalAgd = $ModelStatistic->ToEstimateTotalExpense('agd_cena');
        $totalRozrywka = $ModelStatistic->ToEstimateTotalExpense('fun_cena');
        $totalRachunki = $ModelStatistic->ToEstimateTotalExpense('rachunki_cena');
        $totalAll = $ModelStatistic->ToEstimateTotalExpense('total_cena');

        if($totalAll==0)$totalAll = 1;
        $percentArt = round(($totalArt/$totalAll)*100);
        $percentRtv = round(($totalRtv/$totalAll)*100);
        $percentAgd = round(($totalAgd/$totalAll)*100);
        $percentRoz = round(($totalRozrywka/$totalAll)*100);
        $percentRach = round(($totalRachunki/$totalAll)*100);

        return $this->render('statistic/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'date'=> 'login',
            'fullDate'=> $date->getFullDate(),
            'totalArt'=> $totalArt,
            'totalRtv'=> $totalRtv,
            'totalAgd'=> $totalAgd,
            'totalRozrywka'=> $totalRozrywka,
            'totalRachunki'=> $totalRachunki,
            'totalAll'=> $totalAll,
            'percentArt'=> $percentArt,
            'percentRtv'=> $percentRtv,
            'percentAgd'=> $percentAgd,
            'percentRoz'=> $percentRoz,
            'percentRach'=> $percentRach,
        ]);
    }
}