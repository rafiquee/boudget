<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-23
 * Time: 14:40
 */

namespace AppBundle\Entities;


class DateEntity
{
    private $day;
    private $month;
    private $monthName;
    private $year;
    private $fullDate;

    public function __construct()
    {
        $this->day = date("d");
        $this->month = date("m");
        $this->monthName = date("F");
        $this->year = date("Y");
        $this->fullDate = date("d.m.Y");
    }

    /**
     * @return false|string
     */
    public function getMonthName()
    {
        return $this->monthName;
    }

    /**
     * @param false|string $monthName
     */
    public function setMonthName($monthName)
    {
        $this->monthName = $monthName;
    }

    /**
     * @return false|string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param false|string $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return false|string
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param false|string $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return false|string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param false|string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return false|string
     */
    public function getFullDate()
    {
        return $this->fullDate;
    }

    /**
     * @param false|string $fullDate
     */
    public function setFullDate($fullDate)
    {
        $this->fullDate = $fullDate;
    }


}