<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-28
 * Time: 11:59
 */

namespace AppBundle\Entities;


use AppBundle\Interfaces\EntityInterface;

class RequestBoudgetValueInsertEntity implements EntityInterface
{
    protected $boudgetValue;


    public function getBoudgetValue()
    {
        return $this->boudgetValue;
    }

    public function setBoudgetValue($boudgetValue)
    {
        $this->boudgetValue = $boudgetValue;
        return $this;
    }

    public function isValid(): bool
    {
        if (empty($_POST['boudgetValue'])) return false;

        return true;
    }


    public function toArray(): array
    {
        return ['total' => $this->getBoudgetValue()];
    }

    public function getFields(): array
    {
        return ['total'];
    }

    public function getTableName(): string
    {
        if (isset($_SESSION['login']))
            return '`'.$_SESSION['login'].'.'.'total`';
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }

    public function prepareFromArray(array $params): EntityInterface
    {

        if (isset($params['boudgetValue'])) try {
            $this->setBoudgetValue(new PriceValueObject($params['boudgetValue']));
        } catch (\TypeError $exception) {}

    }
}