<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-20
 * Time: 18:38
 */

namespace AppBundle\Entities;


use AppBundle\Interfaces\EntityInterface;
use AppBundle\TransportObjects\RequestLoginUser;
use AppBundle\ValueObjects\UserNameValueObject;

class RequestLoginUserEntity implements EntityInterface
{
    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @return UserNameValueObject
     */
    public function getLogin(): UserNameValueObject
    {
        return $this->login;
    }

    /**
     * @param UserNameValueObject $login
     * @return RequestLoginUserEntity
     */
    public function setLogin(UserNameValueObject $login): RequestLoginUserEntity
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RequestLoginUserEntity
     */
    public function setPassword(string $password): RequestLoginUserEntity
    {
        $this->password = $password;
        return $this;
    }

    public function toArray(): array
    {
        return ['login' => $this->getLogin()
            ,'password' => $this->getPassword()
        ];
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return ['login','password'];
    }

    public function getTableName(): string
    {
        return 'users';
    }

    /**
     * @param array $params
     * @return EntityInterface
     */
    public function prepareFromArray(array $params): EntityInterface
    {
        if (isset($params['login']))
        {
            try {
                $this->setLogin(new UserNameValueObject($params['login']));
            }catch (\TypeError $exception){}
        }

        if (isset($params['password']))
        {
            $this->setPassword($params['password']);
        }
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        $isValid = true;

        if (empty($this->login))
            $isValid = false;
        if (empty($this->password))
            $isValid = false;

        return $isValid;
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }
}