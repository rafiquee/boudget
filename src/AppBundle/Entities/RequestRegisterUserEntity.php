<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 15:21
 */

namespace AppBundle\Entities;


use AppBundle\Interfaces\EntityInterface;
use AppBundle\ValueObjects\EmailValueObject;
use AppBundle\ValueObjects\UserNameValueObject;

class RequestRegisterUserEntity implements EntityInterface
{
    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $email;

    /**
     * @return UserNameValueObject
     */
    public function getLogin(): UserNameValueObject
    {
        return $this->login;
    }

    /**
     * @param UserNameValueObject $login
     * @return RequestRegisterUserEntity
     */
    public function setLogin(UserNameValueObject $login): RequestRegisterUserEntity
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RequestRegisterUserEntity
     */
    public function setPassword(string $password): RequestRegisterUserEntity
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return EmailValueObject
     */
    public function getEmail(): EmailValueObject
    {
        return $this->email;
    }

    /**
     * @param EmailValueObject $email
     * @return RequestRegisterUserEntity
     */
    public function setEmail(EmailValueObject $email): RequestRegisterUserEntity  //dzięki temu możemy wykorzystać tą zmienną w RequestRegisterUserEntityProviders
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['login' => $this->login,
            'password' => $this->password,
            'email' => $this->email,
            'email_repeat' => $this->email
        ];
    }

    public function getFields(): array
    {
        return ['login', 'password', 'email', 'email_repeat'];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
       return 'users';
    }

    public function prepareFromArray(array $params): EntityInterface
    {
        if (isset($params['login'])) {
            try {
                $this->setLogin(new UsernameValueObject($params['login']));
            } catch (\TypeError $exception) {

            }
        }

        if (isset($params['email'])) {
            try {
                $this->setEmail(new EmailValueObject($params['email']));
            } catch (\TypeError $exception) {
            }
        }
        if (isset($params['password'])) {
            $this->setPassword($params['password']);
        }

    }

    public function isValid(): bool
    {
        $isValid = true;

        if (empty($this->login)) {
            $isValid = false;
        }
        if (empty($this->email)) {
            $isValid = false;
        }
        if (empty($this->password)) {
            $isValid = false;
        }

        return $isValid;
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }
}