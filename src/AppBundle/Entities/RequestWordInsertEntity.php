<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:44
 */

namespace AppBundle\Entities;


use AppBundle\Interfaces\EntityInterface;
use AppBundle\TransportObjects\RequestWordInsert;
use AppBundle\ValueObjects\ArticleValueObject;
use AppBundle\ValueObjects\PriceValueObject;
use AppBundle\ValueObjects\WordValueObject;

class RequestWordInsertEntity implements EntityInterface
{
    protected $art;
    protected $rtv;
    protected $agd;
    protected $fun;
    protected $rach;

    protected $artPrice;
    protected $rtvPrice;
    protected $agdPrice;
    protected $funPrice;
    protected $rachPrice;
    protected $totalPrice;


    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param PriceValueObject $price
     * @return RequestWordInsertEntity
     */
    public function setTotalPrice($price)
    {
        $this->totalPrice = $price;
        return $this;
    }

    public function __construct()
    {
    }

    /**
     * @return ArticleValueObject
     */
    public function getArt(): ArticleValueObject
    {
        return $this->art;
    }

    /**
     * @param ArticleValueObject $art
     * @return RequestWordInsertEntity
     */
    public function setArt(ArticleValueObject $art): RequestWordInsertEntity
    {
        $this->art = $art;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getRtv(): ArticleValueObject
    {
        return $this->rtv;
    }

    /**
     * @param ArticleValueObject $rtv
     * @return RequestWordInsertEntity
     */
    public function setRtv(ArticleValueObject $rtv): RequestWordInsertEntity
    {
        $this->rtv = $rtv;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getAgd(): ArticleValueObject
    {
        return $this->agd;
    }

    /**
     * @param ArticleValueObject $agd
     * @return RequestWordInsertEntity
     */
    public function setAgd(ArticleValueObject $agd): RequestWordInsertEntity
    {
        $this->agd = $agd;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getFun(): ArticleValueObject
    {
        return $this->fun;
    }

    /**
     * @param ArticleValueObject $fun
     * @return RequestWordInsertEntity
     */
    public function setFun(ArticleValueObject $fun): RequestWordInsertEntity
    {
        $this->fun = $fun;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getRach(): ArticleValueObject
    {
        return $this->rach;
    }

    /**
     * @param ArticleValueObject $rach
     * @return RequestWordInsertEntity
     */
    public function setRach(ArticleValueObject $rach): RequestWordInsertEntity
    {
        $this->rach = $rach;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getArtPrice(): PriceValueObject
    {
        return $this->artPrice;
    }

    /**
     * @param PriceValueObject $artPrice
     * @return RequestWordInsertEntity
     */
    public function setArtPrice(PriceValueObject $artPrice): RequestWordInsertEntity
    {
        $this->artPrice = $artPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getRtvPrice(): PriceValueObject
    {
        return $this->rtvPrice;
    }

    /**
     * @param PriceValueObject $rtvPrice
     * @return RequestWordInsertEntity
     */
    public function setRtvPrice(PriceValueObject $rtvPrice): RequestWordInsertEntity
    {
        $this->rtvPrice = $rtvPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getAgdPrice(): PriceValueObject
    {
        return $this->agdPrice;
    }

    /**
     * @param PriceValueObject $agdPrice
     * @return RequestWordInsertEntity
     */
    public function setAgdPrice(PriceValueObject $agdPrice): RequestWordInsertEntity
    {
        $this->agdPrice = $agdPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getFunPrice(): PriceValueObject
    {
        return $this->funPrice;
    }

    /**
     * @param PriceValueObject $funPrice
     * @return RequestWordInsertEntity
     */
    public function setFunPrice(PriceValueObject $funPrice): RequestWordInsertEntity
    {
        $this->funPrice = $funPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getRachPrice(): PriceValueObject
    {
        return $this->rachPrice;
    }

    /**
     * @param PriceValueObject $rachPrice
     * @return RequestWordInsertEntity
     */
    public function setRachPrice(PriceValueObject $rachPrice): RequestWordInsertEntity
    {
        $this->rachPrice = $rachPrice;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['art' => $this->art,
            'art_cena' => $this->artPrice,
            'rtv' => $this->rtv,
            'rtv_cena' => $this->rtvPrice,
            'agd' => $this->agd,
            'agd_cena' => $this->agdPrice,
            'fun' => $this->fun,
            'fun_cena' => $this->funPrice,
            'rachunki' => $this->rach,
            'rachunki_cena' => $this->rachPrice,
            'total_cena' => $this->totalPrice
        ];
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return ['art', 'art_cena', 'rtv', 'rtv_cena', 'agd', 'agd_cena', 'fun', 'fun_cena', 'rachunki', 'rachunki_cena','total_cena'];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        if (isset($_SESSION['login']))
            return $_SESSION['login'];
    }

    /**
     * @param array $params
     * @return EntityInterface
     */
    public function prepareFromArray(array $params): EntityInterface
    {
        if (isset($params['art'])){ try{ $this->setArt(new ArticleValueObject($params['art'])); } catch (\TypeError $exception){} }
        if (isset($params['art_cena'])){ try{ $this->setArtPrice(new PriceValueObject($params['art_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['rtv'])){ try{ $this->setRtv(new ArticleValueObject($params['rtv'])); } catch (\TypeError $exception){} }
        if (isset($params['rtv_cena'])){ try{ $this->setRtvPrice(new PriceValueObject($params['rtv_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['agd'])){ try{ $this->setAgd(new ArticleValueObject($params['agd'])); } catch (\TypeError $exception){} }
        if (isset($params['agd_cena'])){ try{ $this->setAgdPrice(new PriceValueObject($params['agd_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['fun'])){ try{ $this->setFun(new ArticleValueObject($params['fun'])); } catch (\TypeError $exception){} }
        if (isset($params['fun_cena'])){ try{ $this->setFunPrice(new PriceValueObject($params['fun_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['rachunki'])){ try{ $this->setRach(new ArticleValueObject($params['rachunki'])); } catch (\TypeError $exception){} }
        if (isset($params['rachunki_cena'])){ try{ $this->setRachPrice(new PriceValueObject($params['rachunki_cena'])); } catch (\TypeError $exception){} }
        try{ $this->setTotalPrice( $this->getFullPrice($params)); } catch (\TypeError $exception){}

        return $this;
    }

    public function getFullPrice(array $params): int
    {
        if ( (!isset($params['art_cena'])) ) {$params['art_cena'] = 0;}
        if ( (!isset($params['rtv_cena'])) ) {$params['rtv_cena'] = 0;}
        if ( (!isset($params['agd_cena'])) ) {$params['agd_cena'] = 0;}
        if ( (!isset($params['fun_cena'])) ) {$params['fun_cena'] = 0;}
        if ( (!isset($params['rachunki_cena']))) {$params['rachunki_cena'] = 0;}

        return (int)$fullPrice = (int)$params['art_cena'] + (int)$params['rtv_cena'] + (int)$params['agd_cena'] + (int)$params['fun_cena'] + (int)$params['rachunki_cena'];
    }

    public function isValid(): bool
    {
        $isValid = true;

        return $isValid;
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }
}