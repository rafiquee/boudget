<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-25
 * Time: 08:40
 */

namespace AppBundle;


use AppBundle\Entities\RequestWordInsertEntity;
use AppBundle\Lib\EntityManager;
use AppBundle\Lib\MySqlManager;
use AppBundle\TransportObjects\RequestWordInsert;

class IndexModel
{

    function insertRecordToDB(RequestWordInsert $WordToInsert) :void
    {
        $entity = new RequestWordInsertEntity();

        $entity->setArt($WordToInsert->getArt());
        $entity->setArtPrice($WordToInsert->getArtPrice());
        $entity->setRtv($WordToInsert->getRtv());
        $entity->setRtvPrice($WordToInsert->getRtvPrice());
        $entity->setAgd($WordToInsert->getAgd());
        $entity->setAgdPrice($WordToInsert->getAgdPrice());
        $entity->setFun($WordToInsert->getFun());
        $entity->setFunPrice($WordToInsert->getFunPrice());
        $entity->setRach($WordToInsert->getRach());
        $entity->setRachPrice($WordToInsert->getRachPrice());

        $entityManager = (new EntityManager())->insertRecordToActualMonth($WordToInsert);

        if (!$entityManager) {
            die('Error: write in to data base problem');
        }
        unset($_POST);
    }

    function HtmlFormValidator() :bool
    {
        return true;
    }

}