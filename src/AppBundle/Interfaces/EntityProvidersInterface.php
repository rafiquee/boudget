<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 15:07
 */

namespace AppBundle\Interfaces;


interface EntityProvidersInterface
{
    public function prepareFromTransportObject (TransportObjectsInterface $TransportObjectInterface): EntityInterface;

}