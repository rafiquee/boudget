<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:15
 */

namespace AppBundle\Interfaces;


interface TransportObjectsInterface
{

    public function isValid(): bool;

    public function prepareFromArray(array $params);

    public function toArray(): array;

}