<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 09:32
 */

namespace AppBundle\Interfaces;


interface ValueObjectInterface
{
    /**
     * @return mixed
     */
    public function get();

    /**
     * @return int
     */
    public function toInt() :int;

    /**
     * @return mixed
     */
    public function __toString();

}