<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-28
 * Time: 14:32
 */

namespace AppBundle\Lib\EntityProviders;


use AppBundle\Entities\RequestBoudgetValueInsertEntity;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Interfaces\EntityProvidersInterface;
use AppBundle\Interfaces\TransportObjectsInterface;

class RequestBoudgetValueInsertEntityProviders implements EntityProvidersInterface
{

    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
        /**
         * @var RequestLoginUser $requestLoginUser
         */
        $requestBoudgetValue = $TransportObject;
        $en = new RequestBoudgetValueInsertEntity();

        $en->setBoudgetValue( $requestBoudgetValue->getBoudgetValue());

        return $en;
    }
}