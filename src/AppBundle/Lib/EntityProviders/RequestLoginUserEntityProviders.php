<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-20
 * Time: 18:36
 */

namespace AppBundle\Lib\EntityProviders;


use AppBundle\Entities\RequestLoginUserEntity;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Interfaces\EntityProvidersInterface;
use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\TransportObjects\RequestLoginUser;

class RequestLoginUserEntityProviders implements EntityProvidersInterface
{

    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
        /**
         * @var RequestLoginUser $requestLoginUser
         */
        $requestLoginUser = $TransportObject;
        $en = new RequestLoginUserEntity();

        $en->setLogin($requestLoginUser->getLogin());
        $en->setPassword($requestLoginUser->getPassword());

        return $en;
    }
}