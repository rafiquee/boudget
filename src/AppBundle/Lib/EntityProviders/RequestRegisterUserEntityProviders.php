<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 15:11
 */

namespace AppBundle\Lib\EntityProviders;


use AppBundle\Entities\RequestRegisterUserEntity;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Interfaces\EntityProvidersInterface;
use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\TransportObjects\RequestRegisterUser;
use AppBundle\ValueObjects\UserNameValueObject;

class RequestRegisterUserEntityProviders implements EntityProvidersInterface
{

    //funkcja zwraca obiekt, dostosowany do polecenia insert sql w entitymanager;
    public function prepareFromTransportObject(TransportObjectsInterface $TransportObject): EntityInterface
    {
    /** @var RequestRegisterUser $requestRegisterUser */
    $requestRegisterUser = $TransportObject;
    $en = new RequestRegisterUserEntity();

    $en->setLogin($requestRegisterUser->getLogin());
    $en->setPassword($requestRegisterUser->getPassword());
    $en->setEmail($requestRegisterUser->getEmail());

    return $en;
    }

}