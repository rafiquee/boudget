<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:43
 */

namespace AppBundle\Lib\EntityProviders;


use AppBundle\Entities\RequestWordInsertEntity;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Interfaces\EntityProvidersInterface;
use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\TransportObjects\RequestWordInsert;

class RequestWordInsertEntityProviders implements EntityProvidersInterface
{

    public function prepareFromTransportObject(TransportObjectsInterface $WordToInsert): EntityInterface
    {
        /**
         * @var RequestWordInsert $requestWordInsert
         */
        $requestWordInsert = $WordToInsert;
        $entity = new RequestWordInsertEntity();

        $entity->setArt($WordToInsert->getArt());
        $entity->setArtPrice($WordToInsert->getArtPrice());
        $entity->setRtv($WordToInsert->getRtv());
        $entity->setRtvPrice($WordToInsert->getRtvPrice());
        $entity->setAgd($WordToInsert->getAgd());
        $entity->setAgdPrice($WordToInsert->getAgdPrice());
        $entity->setFun($WordToInsert->getFun());
        $entity->setFunPrice($WordToInsert->getFunPrice());
        $entity->setRach($WordToInsert->getRach());
        $entity->setRachPrice($WordToInsert->getRachPrice());
        $entity->setTotalPrice($WordToInsert->getTotalPrice());

        return $entity;
    }
}