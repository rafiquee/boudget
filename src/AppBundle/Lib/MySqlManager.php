<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-23
 * Time: 11:18
 */

namespace AppBundle\Lib;


class MySqlManager
{
    private $handler;
    protected $queryResult;

    private static $instance;

    /**
     * MySQLmanager constructor.
     */
    public function __construct()
    {
        $connection = mysqli_init();
        $connection->real_connect('127.0.0.1', 'root', 'root', 'sokol', 8889);
        $this->handler = $connection;

        return $this;
    }

    /**
     * @param $sql
     * @return MySQLmanager
     */
    public function executeQuery($sql): MySqlManager
    {
        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            //echo "<h3>the operation was successful</h3>";
            return $this;
        }
        else
        {
            echo '<h3>Error: '. $connection->error .'</h3>';
        }
        $connection->close();
    }

    public function oneRecord($sql)
    {
        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            return $result->fetch_row();
        }
    }

    public function fetch_all($sql)
    {

        $connection = $this->handler;

        if ($result = $connection->query($sql))
        {
            return $result->fetch_all();
        }

    }

    /**
     * @return MySQLmanager
     */
    public function getInstance()
    {
        if (empty(self::$instance))
        {
            self::$instance = new MySQLmanager();
            return self::$instance;
        }
    }
}