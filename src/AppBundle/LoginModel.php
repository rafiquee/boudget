<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-24
 * Time: 16:11
 */
namespace AppBundle;

use AppBundle\Entities\RequestLoginUserEntity;
use AppBundle\Lib\EntityManager;
use AppBundle\Lib\MySession;
use AppBundle\Lib\MySqlManager;
use AppBundle\TransportObjects\RequestLoginUser;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginModel
{
    function loginFormValidator() :bool
    {

        $statusLogin = [];
        $statusPassword = [];

        $login = $_POST['login'];
        $password = $_POST['password'];

        if (empty($login)) $statusLogin[] = 'Error: login field is empty';
        if (empty($password)) $statusPassword[] = 'Error: password field is empty';

        //DB level verification
        $Manager = new MySqlManager();
        if (!$result = $Manager->oneRecord("SELECT login FROM users WHERE login = '$login'")) { $statusLogin[] = 'Error: login not registred';}
        else {

            //verify password for this user
            $passwordHash = $Manager->oneRecord("SELECT password FROM users WHERE login='$login'");
            if (!password_verify($password, $passwordHash[0]))
            {
                $statusPassword[] = 'Error: password incorrect';
            }
        }

        if (isset($statusLogin[0])) $_SESSION['login_status'] = $statusLogin[0];
        if (isset($statusPassword[0])) $_SESSION['password_status'] = $statusPassword[0];

        if (isset($statusLogin[0]) || isset($statusPassword[0])) return false; else return true;
    }

    function login(RequestLoginUser $user): void
    {
        $entity = new RequestLoginUserEntity();
        $entity->setPassword($user->getPassword());
        $entity->setLogin($user->getLogin());

        $EntityManager = new EntityManager();
        if ($EntityManager->loginValidator($user))
        {
            if(isset($_SESSION)) session_destroy();
            session_start();
            $session = new Session();
            //destroy current session
            $session->invalidate();
            $session->start();
            $session->set('isLogged', true);
            $session->set('login', $user->getLogin());
            $_SESSION['login'] = $user->getLogin();
        }
        else
        {

            echo "<h3 style='color: red'>Error: The login process has been not completed</h3>";

        }
    }
}