<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-24
 * Time: 09:55
 */

namespace AppBundle;


use AppBundle\Lib\EntityManager;
use AppBundle\Lib\MySqlManager;
use AppBundle\TransportObjects\RequestRegisterUser;
use AppBundle\ValueObjects\EmailValueObject;
use Beeflow\Passwd\Passwd;

class RegisterModel
{

    function passwordVerification($pass)
    {

        $passwordPolicy = array(
            'specialCharsCount' => 0,
            'minimumPasswordLength' => 5,
            'lowerCharsCount' => 1,
            'upperCharsCount' => 0,
            'numbersCount' => 1
        );
        $password = new Passwd($passwordPolicy);

        $isPasswordOk = $password->check($pass);
        return (!$isPasswordOk) ? false : true;

    }

    function register(RequestRegisterUser $user ) :void
    {
        $Manager = new MySQLmanager();
        $password = $user->getPassword();

        if ($this->passwordVerification($password))
        {
            $user->setPassword(password_hash($password, PASSWORD_DEFAULT));

            $entityManager = (new EntityManager())->insert($user);  //insertion to data base

            if (!$entityManager)
            {
                die('The registration process has been not completed');
            }
            else if($entityManager)
            {
                $login = $_POST['login'];
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.January` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT (10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.February` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.March` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.April` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.May` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.June` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.July` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.August` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.September` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.October` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.November` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.December` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `art` VARCHAR(80) NULL , `art_cena` FLOAT(10) NULL , `rtv` VARCHAR(80) NULL , `rtv_cena` FLOAT(10) NULL , `agd` VARCHAR(80) NULL , `agd_cena` FLOAT(10) NULL , `fun` VARCHAR(80) NULL , `fun_cena` FLOAT(10) NULL , `rachunki` VARCHAR(80) NULL , `rachunki_cena` FLOAT(10) NULL , `total_cena` FLOAT(15) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");

                $Manager->executeQuery("CREATE TABLE `sokol`.`$login.total` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `total` FLOAT(10) NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;");

            }
            unset($_POST);

        }else{
            echo "<h3 style='color: red'>The registration process has been not completed</h3>";
        }
    }

    function HtmlFormValidator() :bool
    {
        $statusLogin = [];
        $statusPassword = [];
        $statusEmail = [];
        $statusEmailRepeat = [];

        $login = $_POST['login'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        $emailRepeat = $_POST['email_repeat'];

        if (empty($login)) $statusLogin[] = 'Error: login field is empty';
        if (empty($password)) $statusPassword[] = 'Error: password field is empty';
        if (empty($email)) $statusEmail[] = 'Error: email field is empty';
        if (empty($emailRepeat)) $statusEmailRepeat[] = 'Error: email repeat field is empty';

        //DB level verification
        $Manager = new MySqlManager();
        if ($result = $Manager->oneRecord("SELECT login FROM users WHERE login = '$login'")){ $statusLogin[] = 'Error: this login is already taken'; }
        if (!$this->passwordVerification($password)) { $statusPassword[] = 'Error: password must be composed by </br> minimum 5 chars and include 1 lower </br> char and 1 number'; }
        if ($result = $Manager->oneRecord("SELECT email FROM users WHERE email = '$email'")){ $statusEmail[] = 'Error: this email is already taken'; } else try { $user = new EmailValueObject($_POST['email']); } catch (TypeError $exception) { $statusEmail[] = $exception->getMessage(); }
        if ($email != $emailRepeat) { $statusEmailRepeat[] = 'Error: this field must be thiseme like emeil';}

        //array[0] give us first error reported
        if (isset($statusLogin[0])) { $_SESSION['login_status'] = $statusLogin[0]; }
        if (isset($statusPassword[0])) { $_SESSION['password_status'] = $statusPassword[0]; }
        if (isset($statusEmail[0])) { $_SESSION['email_status'] = $statusEmail[0]; }
        if (isset($statusEmailRepeat[0])) { $_SESSION['email_repeat_status'] = $statusEmailRepeat[0]; }

        if( isset($statusLogin[0]) || isset($statusPassword[0]) || isset($statusEmail[0])  || isset($statusEmailRepeat[0]) ) return false; else return true;
    }
}