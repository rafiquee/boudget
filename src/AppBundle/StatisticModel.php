<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-27
 * Time: 09:27
 */

namespace AppBundle;


use AppBundle\Entities\DateEntity;
use AppBundle\Lib\MySqlManager;
use Symfony\Component\HttpFoundation\Session\Session;

class StatisticModel
{
    protected $Manager;
    protected $session;
    protected $date;

    public function __construct()
    {
        $this->Manager = new MySqlManager();
        $this->session = new Session();
        $this->session->migrate();
        $this->session->getFlashBag()->get('login');
        $this->date = new DateEntity();
    }

    public function ToEstimateTotalExpense(String $tableColumn): int
    {
        $login = $this->session->get('login');
        $totalExpense = 0;

        $total = $this->Manager->fetch_all('SELECT `'.$tableColumn.'` FROM `' . $login . '.' . $this->date->getMonthName() . '`');

        for ($i = 0; $i < count($total); ++$i) { $totalExpense += $total[$i][0]; }

        return $totalExpense;
    }

}