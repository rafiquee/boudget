<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-28
 * Time: 11:48
 */

namespace AppBundle\TransportObjects;


use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\ValueObjects\PriceValueObject;

class RequestBoudgetValueInsert implements TransportObjectsInterface
{
protected $boudgetValue;

    /**
     * @return PriceValueObject
     */
    public function getBoudgetValue(): PriceValueObject
    {
        return $this->boudgetValue;
    }

    /**
     * @param PriceValueObject $boudgetValue
     * @return RequestBoudgetValueInsert
     */
    public function setBoudgetValue(PriceValueObject $boudgetValue): RequestBoudgetValueInsert
    {
        $this->boudgetValue = $boudgetValue;
        return $this;
    }

    public function isValid(): bool
    {
        if(empty($_POST['boudgetValue'])) return false;

        return true;
    }

    public function prepareFromArray(array $params): TransportObjectsInterface
    {
        if(isset($params['boudgetValue'])) try{ $this->setBoudgetValue(new PriceValueObject($params['boudgetValue'])); } catch (\TypeError $exception){}

        return $this;
    }

    public function toArray(): array
    {
        return ['boudgetValue' => $this->getBoudgetValue()];
    }

}