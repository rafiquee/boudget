<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-15
 * Time: 22:19
 */

namespace AppBundle\TransportObjects;

use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\ValueObjects\EmailValueObject;

class RequestRegisterUser extends RequestLoginUser implements TransportObjectsInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $email_repeat;

    public function __construct()
    {
    }

    /**
     * @return EmailValueObject
     */
    public function getEmail():EmailValueObject
    {
        return new EmailValueObject($this->email);
    }

    /**
     * @param EmailValueObject $email
     * @return RequestRegisterUser
     */
    public function setEmail(EmailValueObject $email): RequestRegisterUser
    {
        $this->email = $email;
        return $this;
    }

    public function getEmailRepeat()
    {
        return $this->email_repeat;
    }

    /**
     * @param mixed $email_repeat
     * @return mixed
     */
    public function setEmailRepeat($email_repeat)
    {
        $this->email_repeat = $email_repeat;
    }

    //empty fields validator
    public function isValid(): bool
    {
        $status = true;

        if ( (empty($this->email)) || (empty($this->email_repeat)) )
        {
            $status = false;
        }

        return $status && parent::isValid();
    }

    public function prepareFromArray(array $params): TransportObjectsInterface
    {
        if (isset($params['email']))
        {
            try
            {
                $this->setEmail(new EmailValueObject($params['email']));
            }
            catch (\TypeError $exception){}
        }

        if ( (isset($params['email_repeat'])) && ($params['email_repeat'] == $params['email']) )
        {
            $this->setEmailRepeat($params['email_repeat']);
        }

        // include results prepareFromArray()::RequestLoginUser
        parent::prepareFromArray($params);
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = parent::toArray();
        $array['email'] = $this->email;
        return $array;
    }
}