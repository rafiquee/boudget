<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-22
 * Time: 08:42
 */

namespace AppBundle\TransportObjects;

use AppBundle\Interfaces\TransportObjectsInterface;
use AppBundle\ValueObjects\ArticleValueObject;
use AppBundle\ValueObjects\PriceValueObject;

class RequestWordInsert implements TransportObjectsInterface
{

    protected $art;
    protected $rtv;
    protected $agd;
    protected $fun;
    protected $rach;

    protected $artPrice;
    protected $rtvPrice;
    protected $agdPrice;
    protected $funPrice;
    protected $rachPrice;
    protected $totalPrice;


    public function getTotalPrice()
    {
        return $this->totalPrice;
    }


    public function setTotalPrice($price)
    {
        $this->totalPrice = $price;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getArt(): ArticleValueObject
    {
        return $this->art;
    }

    /**
     * @param ArticleValueObject $art
     * @return RequestWordInsert
     */
    public function setArt(ArticleValueObject   $art): RequestWordInsert
    {
        $this->art = $art;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getRtv(): ArticleValueObject
    {
        return $this->rtv;
    }

    /**
     * @param ArticleValueObject $rtv
     * @return RequestWordInsert
     */
    public function setRtv(ArticleValueObject $rtv): RequestWordInsert
    {
        $this->rtv = $rtv;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getAgd(): ArticleValueObject
    {
        return $this->agd;
    }

    /**
     * @param ArticleValueObject $agd
     * @return RequestWordInsert
     */
    public function setAgd(ArticleValueObject $agd): RequestWordInsert
    {
        $this->agd = $agd;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getFun(): ArticleValueObject
    {
        return $this->fun;
    }

    /**
     * @param ArticleValueObject $fun
     * @return RequestWordInsert
     */
    public function setFun(ArticleValueObject $fun): RequestWordInsert
    {
        $this->fun = $fun;
        return $this;
    }

    /**
     * @return ArticleValueObject
     */
    public function getRach(): ArticleValueObject
    {
        return $this->rach;
    }

    /**
     * @param ArticleValueObject $rach
     * @return RequestWordInsert
     */
    public function setRach(ArticleValueObject $rach): RequestWordInsert
    {
        $this->rach = $rach;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getArtPrice(): PriceValueObject
    {
        return $this->artPrice;
    }

    /**
     * @param PriceValueObject $artPrice
     * @return RequestWordInsert
     */
    public function setArtPrice(PriceValueObject $artPrice): RequestWordInsert
    {
        $this->artPrice = $artPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getRtvPrice(): PriceValueObject
    {
        return $this->rtvPrice;
    }

    /**
     * @param PriceValueObject $rtvPrice
     * @return RequestWordInsert
     */
    public function setRtvPrice(PriceValueObject $rtvPrice): RequestWordInsert
    {
        $this->rtvPrice = $rtvPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getAgdPrice(): PriceValueObject
    {
        return $this->agdPrice;
    }

    /**
     * @param PriceValueObject $agdPrice
     * @return RequestWordInsert
     */
    public function setAgdPrice(PriceValueObject $agdPrice): RequestWordInsert
    {
        $this->agdPrice = $agdPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getFunPrice(): PriceValueObject
    {
        return $this->funPrice;
    }

    /**
     * @param PriceValueObject $funPrice
     * @return RequestWordInsert
     */
    public function setFunPrice(PriceValueObject $funPrice): RequestWordInsert
    {
        $this->funPrice = $funPrice;
        return $this;
    }

    /**
     * @return PriceValueObject
     */
    public function getRachPrice(): PriceValueObject
    {
        return $this->rachPrice;
    }

    /**
     * @param PriceValueObject $rachPrice
     * @return RequestWordInsert
     */
    public function setRachPrice(PriceValueObject $rachPrice): RequestWordInsert
    {
        $this->rachPrice = $rachPrice;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['art' => $this->art,
            'art_cena' => $this->artPrice,
            'rtv' => $this->rtv,
            'rtv_cena' => $this->rtvPrice,
            'agd' => $this->agd,
            'agd_cena' => $this->agdPrice,
            'fun' => $this->fun,
            'fun_cena' => $this->funPrice,
            'rachunki' => $this->rach,
            'rachunki_cena' => $this->rachPrice,
            'total_cena' => $this->totalPrice
        ];
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return ['art', 'art_cena', 'rtv', 'rtv_cena', 'agd', 'agd_cena', 'fun', 'fun_cena', 'rachunki', 'rachunki_cena', 'total_cena'];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        if (isset($_SESSION['login']))
            return $_SESSION['login'];
    }

    /**
     * @param array $params
     * @return TransportObjectsInterface
     */
    public function prepareFromArray(array $params): TransportObjectsInterface
    {

        if (isset($params['art'])){ try{ $this->setArt(new ArticleValueObject($params['art'])); } catch (\TypeError $exception){} }
        if (isset($params['art_cena'])){ try{ $this->setArtPrice(new PriceValueObject($params['art_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['rtv'])){ try{ $this->setRtv(new ArticleValueObject($params['rtv'])); } catch (\TypeError $exception){} }
        if (isset($params['rtv_cena'])){ try{ $this->setRtvPrice(new PriceValueObject($params['rtv_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['agd'])){ try{ $this->setAgd(new ArticleValueObject($params['agd'])); } catch (\TypeError $exception){} }
        if (isset($params['agd_cena'])){ try{ $this->setAgdPrice(new PriceValueObject($params['agd_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['fun'])){ try{ $this->setFun(new ArticleValueObject($params['fun'])); } catch (\TypeError $exception){} }
        if (isset($params['fun_cena'])){ try{ $this->setFunPrice(new PriceValueObject($params['fun_cena'])); } catch (\TypeError $exception){} }
        if (isset($params['rachunki'])){ try{ $this->setRach(new ArticleValueObject($params['rachunki'])); } catch (\TypeError $exception){} }
        if (isset($params['rachunki_cena'])){ try{ $this->setRachPrice(new PriceValueObject($params['rachunki_cena'])); } catch (\TypeError $exception){} }
        try{ $this->setTotalPrice( $this->getFullPrice($params) ); } catch (\TypeError $exception){}

        return $this;
    }

    public function getFullPrice(array $params): int
    {
        if ( (!isset($params['art_cena'])) ) {$params['art_cena'] = 0;}
        if ( (!isset($params['rtv_cena'])) ) {$params['rtv_cena'] = 0;}
        if ( (!isset($params['agd_cena'])) ) {$params['agd_cena'] = 0;}
        if ( (!isset($params['fun_cena'])) ) {$params['fun_cena'] = 0;}
        if ( (!isset($params['rachunki_cena']))) {$params['rachunki_cena'] = 0;}

        return (int)$fullPrice = (int)$params['art_cena'] + (int)$params['rtv_cena'] + (int)$params['agd_cena'] + (int)$params['fun_cena'] + (int)$params['rachunki_cena'];
    }

    public function isValid(): bool
    {
        $isValid = true;

        return $isValid;
    }

    public function getIdName(): string
    {
        // TODO: Implement getIdName() method.
    }
}

