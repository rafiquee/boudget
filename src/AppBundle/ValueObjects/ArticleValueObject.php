<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-26
 * Time: 14:58
 */

namespace AppBundle\ValueObjects;


use AppBundle\Interfaces\ValueObjectInterface;

class ArticleValueObject implements ValueObjectInterface
{

    protected $word;
    /**
     * WordValueObject constructor.
     * @param string $word
     */
    public function __construct($word)
    {

        if (empty($word)) return $this->word = '-';

        return $this->word = $word;

    }

    public function get()
    {

        return $this->word;

    }

    /**
     * return int
     * @throws \TypeError
     */
    public function toInt(): int
    {

        throw new \TypeError('Error: enter valid word');

    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->word;
    }
}