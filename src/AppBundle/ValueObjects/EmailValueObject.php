<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-16
 * Time: 13:21
 */

namespace AppBundle\ValueObjects;


use AppBundle\CommonObjects\CommonValueObjects;
use AppBundle\Interfaces\ValueObjectInterface;

class EmailValueObject extends CommonValueObjects implements ValueObjectInterface
{
    /**
     * EmailValueObject constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //throw new \TypeError('email incorrect');
            $_SESSION['email_status'] = 'email incorrect';
        }

        if (strpos( $email,'#') !== false) {
            //throw new \TypeError('email incorrect');
            $_SESSION['email_status'] = 'email incorrect';
        }

        $this->value = $email; //properties assigned by abstract class CommonValueObjects
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function toInt(): int
    {
        // TODO: Implement toInt() method.
    }
}