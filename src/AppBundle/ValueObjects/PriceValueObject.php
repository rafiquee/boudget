<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2019-01-26
 * Time: 10:56
 */

namespace AppBundle\ValueObjects;


use AppBundle\CommonObjects\CommonValueObjects;
use AppBundle\Interfaces\ValueObjectInterface;

class PriceValueObject implements ValueObjectInterface
{

    protected $price;

    public function __construct($price)
    {
        if (empty($price))
            $price = (int)0;

        return $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function get()
    {
       return $this->price;
    }

    /**
     * @return int
     */
    public function toInt(): int
    {
       return $this->price;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        throw new \TypeError('must bee number');
    }
}